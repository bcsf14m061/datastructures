
#ifndef QUEUE_H
#define QUEUE_H

template<class T>
class Queue;

template<class T>
class queNode
{
public:
	T val;
	queNode<T>* next;
	queNode(T v);
	queNode(T v, queNode<T>* n);
	template <class T>
	friend class Queue;
};
template<class T>
class Queue
{
	queNode<T>* front;
	queNode<T>* rear;
public:
	Queue();
	bool isEmpty();
	void Enqueue(const T v);
	T dequeue();
};


#endif