#include "AVL.h"

AVL::AVL()
{
	root = NULL;
}

int AVL::findMax(int p, int q)
{
	if (p < q)
		return q;
	else
		return p;
}

avlNode* AVL::findMaxNode(avlNode* t)
{
	avlNode* curr = t;
	while (curr->right != NULL)
	{
		curr = curr->right;
	}
	return curr;
}

avlNode* AVL::parentNode(int d)
{
	avlNode* prt = NULL;
	if (root == NULL)
	{
		return NULL;
	}
	else
	{
		avlNode* c = root;
		while (c != NULL)
		{
			if (c->data > d)
			{
				prt = c;
				c = c->left;

			}
			else if (c->data < d)
			{
				prt = c;
				c = c->right;

			}
			else if (c->data == d)
				return prt;
		}
		return NULL;
	}
}

void AVL::balanceHeight(avlNode* p)
{
	if (p != NULL)
	{
		if (p->left != NULL && p->right != NULL)
		{
			p->height = findMax(p->left->height, p->right->height) + 1;
		}
		else if (p->left != NULL && p->right == NULL)
		{
			p->height = p->left->height + 1;
		}
		else if (p->right != NULL && p->left == NULL)
		{
			p->height = p->right->height + 1;
		}
		else if (p->left == NULL && p->right == NULL)
		{
			p->height = 0;
		}
	}
	else
		return;
}

void AVL::RotateRight(avlNode* p, avlNode* q)
{
	avlNode* k = p->left;
	p->left = k->right;
	k->right = p;
	if (q == NULL)
	{
		root = k;
		balanceHeight(root);
	}
	else if (q != NULL)
	{
		if (p == q->left)
			q->left = k;
		else if (p == q->right)
			q->right = k;
	}
	balanceHeight(p);
	balanceHeight(k);
}

void AVL::RotateLeft(avlNode* p, avlNode* q)
{
	avlNode* k = p->right;
	p->right = k->left;
	k->left = p;
	if (q == NULL)
	{
		root = k;
		balanceHeight(root);
	}
	else if (q != NULL)
	{
		if (p == q->left)
			q->left = k;
		else if (p == q->right)
			q->right = k;
	}
	balanceHeight(p);
	balanceHeight(k);
}

bool AVL::insert(int d)
{
	avlNode* t = new avlNode(d);
	Stack<avlNode*> s;
	avlNode* c = NULL;
	avlNode* p = NULL;
	if (root == NULL)
	{
		root = t;
		return true;
	}
	else
	{
		c = root;
		while (c != NULL)
		{
			if (d < c->data)
			{
				p = c;
				c = c->left;
			}
			else if (d > c->data)
			{
				p = c;
				c = c->right;
			}
			else
				return false;
			s.push(p);
		}
		if (d < p->data)
		{
			p->left = t;
		}
		else if (d > p->data)
		{
			p->right = t;
		}
	}

	while (!s.isEmpty())
	{
		avlNode* temp = s.pop();
		int val;
		avlNode* temp1 = NULL;
		if (!s.isEmpty())
			temp1 = s.pop();
		if (temp->right != NULL && temp->left != NULL)
		{
			val = ((temp->left->height) - (temp->right->height));
		}
		else if (temp->right != NULL && temp->left == NULL)
		{
			val = (-1) - temp->right->height;
		}
		else if (temp->left != NULL && temp->right == NULL)
		{
			val = (temp->left->height) - (-1);
		}

		if (val ==0||val==1 || val == -1)
		{
			balanceHeight(temp);
		}
		else
		{
			if (val >= 2)
			{
				if (temp->left->left != NULL && temp->left->right != NULL)
				{
					if (temp->left->left->height > temp->left->right->height)
						RotateRight(temp, temp1);
					else if (temp->left->left->height < temp->left->right->height)
					{
						RotateLeft(temp->left, temp);
						RotateRight(temp, temp1);
					}
				}
				else if (temp->left->right == NULL && temp->left->left != NULL)
				{
					RotateRight(temp, temp1);
				}
			}
			else if (val <= -2)
			{
				if (temp->right->right != NULL && temp->right->left != NULL)
				{
					if (temp->right->right->height > temp->right->left->height)
					{
						RotateLeft(temp, temp1);
					}
					else if (temp->right->left->height > temp->right->right->height)
					{
						RotateRight(temp->right, temp);
						RotateLeft(temp, temp1);
					}
				}
				else if (temp->right->right != NULL && temp->right->left == NULL)
					RotateLeft(temp, temp1);
			}

			balanceHeight(temp);
			balanceHeight(temp1);
		}
		/*if (temp1 != NULL)
			s.push(temp1);*/
	}

	return true;
}

avlNode* AVL::Search(avlNode* p, int d)
{
	if (p == NULL)
	{
		return NULL;
	}
	else
	{
		if (p->data == d)
		{
			return p;
		}
		else if (p->data > d)
		{
			Search(p->left, d);
		}
		else if (p->data < d)
		{
			Search(p->right, d);
		}
	}
}

avlNode* AVL::Search(int d)
{
	return Search(root, d);
}

void AVL::printSideWays(avlNode* t,int sp)
{
	if (t == NULL)
	{
		cout << "\nTree is Empty...";
	}
	else
	{
		if (t->right != NULL)
		{
			printSideWays(t->right, sp + 1);
		}
		for (int i = 0; i < sp; i++)
		{
			cout << "\t";
		}
		cout << t->data;
		cout << endl;
		if (t->left != NULL)
		{
			printSideWays(t->left, sp + 1);
		}
	}
}

void AVL::printSideWays(int sp)
{
	printSideWays(root, sp);
}

void AVL::LevelOrderDisp()
{
	Queue<avlNode*> queue;
	avlNode* temp = root;
	while (temp != NULL)
	{
		cout << temp->data;
		cout << " ";
		if (temp->left != NULL)
			queue.Enqueue(temp->left);
		if (temp->right != NULL)
			queue.Enqueue(temp->right);
		temp = queue.dequeue();
	}
}

bool AVL::deletion(int d)
{
	Stack<avlNode*> s;
	avlNode* p = NULL;
	bool flag = false;
	avlNode* curr = NULL;
	if (root != NULL)
	{
	    curr = root;
		while (curr != NULL && flag == false)
		{
			if (curr->data > d)
			{
				p = curr;
				curr = curr->left;
				s.push(p);
			}
			else if (curr->data < d)
			{
				p = curr;
				curr = curr->right;
				s.push(p);
			}
			else if (curr->data == d)
			{
				flag = true;
				break;
			}
		}
	}
	if (root == NULL)
		return false;
    if (flag == false)
		return false;
	if (curr->right != NULL && curr->left != NULL)
	{
		avlNode* t = NULL;
		t = findMaxNode(curr->left);
		avlNode* pt = NULL;
		pt = parentNode(t->data);
		int val = curr->data;
		curr->data = t->data;
		t->data = val;
		curr = t;
		p = pt;
	}
	if (curr->right == NULL && curr->left == NULL)
	{
		if (p == NULL)
			root = NULL;
		else if (curr == p->right)
			p->right = NULL;
		else if (curr == p->left)
			p->left = NULL;
		delete curr;
		balanceHeight(p);
	}

	else if (curr->right == NULL && curr->left != NULL)
	{
		if (p == NULL)
			root = curr->left;
		else if (curr == p->right)
			p->right = curr->left;
		else if (curr == p->left)
			p->left = curr->left;
		delete curr;
	}
	else if (curr->right != NULL && curr->left == NULL)
	{
		if (p == NULL)
			root = curr->right;
		else if (curr == p->right)
			p->right = curr->right;
		else if (curr == p->left)
			p->left = curr->right;
		delete curr;
	}
	while (!s.isEmpty())
	{
		avlNode* temp = s.pop();
		int val;
		avlNode* temp1 = NULL;
		if (!s.isEmpty())
			temp1 = s.pop();
		if (temp->right != NULL && temp->left != NULL)
		{
			val = ((temp->left->height) - (temp->right->height));
		}
		else if (temp->right != NULL && temp->left == NULL)
		{
			val = (-1) - temp->right->height;
		}
		else if (temp->left != NULL && temp->right == NULL)
		{
			val = (temp->left->height) - (-1);
		}

		if (val <= 1 && val >= -1)
		{
			balanceHeight(temp);
		}
		else
		{
			if (val >= 2)
			{
				if (temp->left->left != NULL && temp->left->right != NULL)
				{
					if (temp->left->left->height > temp->left->right->height)
						RotateRight(temp, temp1);
					else
					{
						RotateLeft(temp->left, temp);
						RotateRight(temp, temp1);
					}
				}
				else if (temp->left->right == NULL && temp->left->left != NULL)
				{
					RotateRight(temp, temp1);
				}
			}
			else if (val <= -2)
			{
				if (temp->right->right != NULL && temp->right->left != NULL)
				{
					if (temp->right->right->height > temp->right->left->height)
					{
						RotateLeft(temp, temp1);
					}
					else if (temp->right->left->height > temp->right->right->height)
					{
						RotateRight(temp->right, temp);
						RotateLeft(temp, temp1);
					}
				}
				else if (temp->right->right != NULL && temp->right->left == NULL)
					RotateLeft(temp, temp1);
			}

			balanceHeight(temp);
			balanceHeight(temp1);
		}
		if (temp1 != NULL)
			s.push(temp1);
	}
	return flag;
}

void AVL::clear(avlNode* t)
{
	if (t == NULL)
		return;
	else
	{
		clear(t->left);
		clear(t->right);
		delete t;
	}
}

AVL::~AVL()
{
	clear(root);
	root = NULL;
}
