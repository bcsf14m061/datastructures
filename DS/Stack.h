
#ifndef STACK_H
#define STACK_H

template<class T>
class Stack;

template<class T>
class Node
{
	T val;
	Node<T>*next;
	template<class x>
	friend class Stack;
public:
	Node(T v=0,Node<T>* n=0);

};

template<class T>
class Stack
{
	Node<T>* top;
public:
	Stack();
	int isEmpty();
	void push(T v);
	T pop();
	T seek();

};

#endif