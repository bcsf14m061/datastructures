#include<iostream>
#include"Stack.h"
#include"Stack.cpp"
#include"Queue.h"
#include"Queue.cpp"
using namespace std;

#ifndef AVL_H
#define AVL_H

class avlNode
{
	avlNode* left;
	avlNode* right;
	//int data;
	int height;
	friend class AVL;
public:
	int data; // Just to check the working of search function in main
	avlNode(int d = 0)
	{
		data = d;
		height = 0;
		left = NULL;
		right = NULL; 
	}
};

class AVL
{
	avlNode* root;
public:
	AVL();
	int findMax(int, int);
	avlNode* findMaxNode(avlNode* );
	avlNode* parentNode(int );
	void balanceHeight(avlNode* );
	void  RotateRight(avlNode* , avlNode* );
	void RotateLeft(avlNode* , avlNode* );
	bool insert(int );
	avlNode* Search(avlNode* , int );
	avlNode* Search(int ); // driver function
	void printSideWays(avlNode* ,int );
	void printSideWays(int );
	void LevelOrderDisp();
	void clear(avlNode* );
	~AVL();
	bool deletion(int );
	
};
#endif