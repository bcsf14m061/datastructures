#include"AVL.h"

int main()
{
	AVL tree;
	//int d;
	//char choice;
	//cout << "\n1. Checking the Insert Function...\n";
	//do
	//{
	//	cout << "\nEnter the integer data you want to insert in the Tree: ";
	//	cin >> d;
	//	if (tree.insert(d))
	//	{
	//		cout << "\nInsertion Successful...\nDo you want to insert more? (Y/N):  ";
	//		cin >> choice;
	//		if (choice == 'N' || choice == 'n')
	//		{
	//			cout << endl ;
	//			cout << "--------------------------------------------------------------------------\n";
	//			cout << "2. Printing the Tree in SideWays Order...\n\n";
	//			tree.printSideWays(0);
	//		}
	//	}
	//	else
	//		cout << "\nThis data cannot be entered in the Tree.";
	//} while (choice == 'Y' || choice == 'y');

	tree.insert(15);
	tree.insert(18);
	tree.insert(0);
	tree.insert(-4);
	tree.insert(2);
	tree.insert(9);

	tree.printSideWays(0);
	int num;
	cout << "--------------------------------------------------------------------------\n";
	cout << "3. Checking the Search Function...\n";
	cout << "Enter the data you want to search in the Tree: ";
	cin >> num;
	avlNode* t = tree.Search(num);
	if (t != NULL)
	{
		cout << "\nSearched Data: " << t->data << endl;
	}
	else
	{
		cout << "\nThis Data isn't present in the Tree.\n";
	}
	int node;
	cout << "--------------------------------------------------------------------------";
	cout << "\n4. Removing data from tree...\n";
	cout << "\nEnter the Data you want to remove from the Tree: ";
	cin >> node;
	tree.deletion(node);
	tree.printSideWays(0);
	cout << "--------------------------------------------------------------------------\n";
	cout << "\n5. Data of Tree in Level Order...\n\n" << " ";
	tree.LevelOrderDisp();
	cout << endl << endl;
	return 0;
}