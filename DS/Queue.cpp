#include "Queue.h"

template <class T>
queNode<T>::queNode(T d)
{
	val = d;
	next = 0;

}

template <class T>
queNode<T>::queNode(T d,queNode<T>* n)
{
	val = d;
	next = n;

}

template<class T>
Queue<T>::Queue()
{
	front = NULL;
	rear = NULL;
}

template<class T>
bool Queue<T>::isEmpty()
{
	if (front == NULL)
		return true;
	else
		return false;
}

template <class T>
void Queue<T>::Enqueue(const T d)
{
	queNode<T>* t = new queNode<T>(d);
	if (front == NULL)
	{
		front = t;
		rear = t;
	}
	else
	{
		rear->next = t;
		rear = t;
	}
}

template<class T>
T Queue<T>::dequeue()
{
	if (front != NULL)
	{
		T x;
		x = front->val;
		queNode<T> * t = front;
		front = front->next;
		delete t;
		return x;
	}
	else
	{
		exit(1);
	}
}