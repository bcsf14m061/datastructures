#include "Stack.h"
#include<iostream>

template <class T>
Node<T>::Node(T v, Node<T>* n)
{
	val = v;
	next = n;
}

template<class T>
Stack<T>::Stack()
{
	top = NULL;
}

template<class T>
int Stack<T>:: isEmpty()
{
	if (top == NULL)
		return 1;
	else
		return 0;
}

template<class T>
void Stack<T>::push(T v)
{
	Node<T>* t = new Node<T>(v);
	t->next = top;
	top = t;
}

template<class T>
T Stack<T>::pop()
{
	if(!isEmpty())
	{
		Node<T>* p = top;
		Node<T>* after = top->next;
		T val = p->val;
		delete p;
		top = after;
		return val;
	}
	else
		return NULL;
}

template<class T>
T Stack<T>::seek()
{
	if (!isEmpty())
		return top->val;
	else
	{
		return NULL;
	}
}